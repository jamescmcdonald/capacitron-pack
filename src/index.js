import "./style.css";

const cspMeta = document.createElement("meta");
cspMeta.httpEquiv = "Content-Security-Policy";
cspMeta.content = "script-src 'self' 'unsafe-inline';";

const mainSection = document.createElement("section");
const mainSectionHeading = document.createElement("h1");
const mainSectionBodyText = document.createElement("p");
const mainSectionStorageText = document.createElement("p");

mainSectionHeading.appendChild(new Text("Capacitron-Pack Demo"));
mainSectionBodyText.appendChild(new Text("Text added by index.js"));

document.head.appendChild(cspMeta);
document.body.appendChild(mainSection);

mainSection.appendChild(mainSectionHeading);
mainSection.appendChild(mainSectionBodyText);
mainSection.appendChild(mainSectionStorageText);

function formatBytes(bytes, decimals = 2) {
  if (bytes === 0) return "0 Bytes";

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

if (navigator && "storage" in navigator) {
  navigator.storage
    .estimate()
    .then((value) =>
      mainSectionStorageText.appendChild(
        new Text(`Storage estimate is ${formatBytes(value.quota)}`)
      )
    );
} else {
  mainSectionStorageText.appendChild(new Text(`Can't estimate storage`));
}
